
const BASE_URL ="https://62db6db0e56f6d82a7729204.mockapi.io";

// lấy danh sách trên axios
function getDSSV () {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then(function (res) {
            tatLoading();
            console.log(res);
            renderDSSV(res.data);
        })
        .catch(function (err) {
            tatLoading();
            console.log(err);
        });
}
// lấy danh sách lần đầu tiên
getDSSV();


// thêm sinh viên
function themSV() {
    // batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: layThongTinTuForm(),
    })
        .then(function (res) {
            console.log(res);
            // lấy danh sách khi thành công
            getDSSV();
        })
        .catch(function (err) {
            console.log(err);
        });
}

// xóa sinh viên
function xoaSV (id) {
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            console.log(res);
            // lấy danh sách khi thành công
            getDSSV();
        })
        .catch(function (err) {
            console.log(err);
        });
};


// sua sinh vien
function suaSV (id) {
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "GET",
    })
        .then(function (res) {
            console.log(res);
            var sinhVienCanSua = res.data;
            showThongTinLenForm(sinhVienCanSua);
         
        })
        .catch(function (err) {
            console.log(err);
        });
};

// cap nhat

function capNhat() {
    var doiTuongDaSua = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/sv/${doiTuongDaSua.id}`,
        method: "PUT",
        data: doiTuongDaSua,
    })
        .then(function (res) {
            console.log(res);
           getDSSV();
         
        })
        .catch(function (err) {
            console.log(err);
        });
}

function timKiemTheoTen() {
    var tenCanTim = document.getElementById('txtSearch').value;
    var doiTuongTimDuoc = [];
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
    .then(function (res) {
        console.log(res);
        var mangDoiTuong = res.data;
        console.log('tenCanTim: ', tenCanTim);
        
        for ( var i = 0; i < mangDoiTuong.length; i++ ) {
            if ( mangDoiTuong[i].name.includes(tenCanTim) ) {
                doiTuongTimDuoc.push(mangDoiTuong[i]);
            }
        }
        
        // console.log('doiTuongTimDuoc: ', doiTuongTimDuoc);
        renderDSSV(doiTuongTimDuoc);
         
        })
        .catch(function (err) {
            console.log(err);
        });
}



function resetSV() {
    document.getElementById("txtMaSV").value = ""; 
    document.getElementById("txtTenSV").value = ""; 
    document.getElementById("txtEmail").value = ""; 
    document.getElementById("txtPass").value = ""; 
    document.getElementById("txtDiemToan").value = ""; 
    document.getElementById("txtDiemLy").value = ""; 
    document.getElementById("txtDiemHoa").value = ""; 

    getDSSV();

}
