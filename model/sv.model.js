function  SinhVien (ma, ten, email, matKhau, diemToan, diemLy, diemHoa) {
    this.id = ma;
    this.name = ten;
    this.email = email;
    this.pass = matKhau;
    this.math = diemToan;
    this.physics = diemLy;
    this.chemistry = diemHoa;
    this.mediumScore = function() {
        return (this.math + this.physics + this.chemistry) / 3;
    };
}